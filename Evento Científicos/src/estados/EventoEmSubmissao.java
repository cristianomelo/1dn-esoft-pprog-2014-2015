
package estados;

import eventoscientificos.model.EstadosEvento;
import eventoscientificos.model.Evento;

/**
 *
 * @author Utilizador
 */
public class EventoEmSubmissao implements EstadosEvento{
    private Evento m_evento;
    public EventoEmSubmissao(Evento m_evento){
        this.m_evento=m_evento;
    }

    @Override
    public boolean valida() {
       return true;
    }

    @Override
    public EventoCriadoState setCriadoState() {
      return null;
    }

    @Override
    public EventoRegistadoState setRegistadoState() {
        return null;
    }

    @Override
    public EventoSessoesDefenidasState setSessoesDefenidasState() {
       return null;
    }

    @Override
    public EventoCPDefenidaState setCPDefenidaState() {
        return null;
    }

    @Override
    public EventoEmSubmissao setEventoEmSubmissaoState() {
        return null;
    }

    @Override
    public EventoEmLicitacaoState setEventoEmLicitacaoState() {
        return null;
    }

    @Override
    public EventoEmRevisaoState setEventoEmRevisaoState() {
        return null;
    }

    @Override
    public EventoDecedidoState setEventoDecedidoState() {
        return null;
    }

    @Override
    public EventoDistribuidoState setEventoDistribuidoState() {
        return null;
    }

    @Override
    public EventoDetetadoState setEventoEmDetecao() {
        if(valida()){
            m_evento.setEstado(new EventoDetetadoState(m_evento));
        }
        return null;
    }
}
