/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import eventoscientificos.model.EstadosEvento;
import eventoscientificos.model.Evento;

/**
 *
 * @author Utilizador
 */
public class EventoCriadoState implements EstadosEvento{
    private Evento m_Evento;
    public EventoCriadoState(Evento m_evento){
        this.m_Evento=m_evento;
    }
    @Override
    public boolean valida() {
        return true;
    }

    @Override
    public EventoCriadoState setCriadoState() {
        return null;
    }

    @Override
    public EventoRegistadoState setRegistadoState() {
        if(valida()){
         m_Evento.setEstado(new EventoRegistadoState(m_Evento));
        }
        return null;
    }

    @Override
    public EventoSessoesDefenidasState setSessoesDefenidasState() {
       return null;
    }

    @Override
    public EventoCPDefenidaState setCPDefenidaState() {
        return null;
    }

    @Override
    public EventoEmSubmissao setEventoEmSubmissaoState() {
        return null;
    }

    @Override
    public EventoEmLicitacaoState setEventoEmLicitacaoState() {
        return null;
    }

    @Override
    public EventoEmRevisaoState setEventoEmRevisaoState() {
        return null;
    }

    @Override
    public EventoDecedidoState setEventoDecedidoState() {
        return null;
    }

    @Override
    public EventoDistribuidoState setEventoDistribuidoState() {
        return null;
    }

    @Override
    public EventoDetetadoState setEventoEmDetecao() {
        return null;
    }
    
}
