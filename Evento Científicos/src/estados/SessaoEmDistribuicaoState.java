/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import eventoscientificos.model.EstadoSessao;
import eventoscientificos.model.Sessao;

/**
 *
 * @author Utilizador
 */
public class SessaoEmDistribuicaoState implements EstadoSessao{
     private Sessao m_sessao;

    public SessaoEmDistribuicaoState(Sessao mSessao) {
        this.m_sessao = mSessao;
    }

    @Override
    public boolean valida() {
        return true;
    }

    @Override
    public SessaoCriadaState setCriadoState() {
        return null;
    }

    @Override
    public SessaoRegistadaState setRegistadaState() {

        return null;
    }

    @Override
    public SessaoCPDefenidaState setCPDefenidaState() {
        return null;
    }

    @Override
    public SessaoEmSubmissaoState setEmSubmissaoState() {
        return null;
    }

    @Override
    public SessaoEmLicitacaoState setEmLicitacaoState() {
        return null;
    }

    @Override
    public SessaoEmDistribuicaoState setEmDistribuicaoState() {
        return null;
    }

    @Override
    public SessaoDecedidaState setDecedidoState() {
        return null;
    }

    @Override
    public SessaoDetetadaState setDetetadoState() {
       
        return null;
    }

    @Override
    public SeessaoEmRevisaoState setEmRevisao() {
           if(valida()){
            m_sessao.setEstadoSessao(new SeessaoEmRevisaoState(m_sessao));
        } 
        return null;
    }
}
