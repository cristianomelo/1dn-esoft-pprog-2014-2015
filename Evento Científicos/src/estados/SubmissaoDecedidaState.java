/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import eventoscientificos.model.EstadosSubmissao;
import eventoscientificos.model.Submissao;

/**
 *
 * @author Utilizador
 */
public class SubmissaoDecedidaState implements EstadosSubmissao {

    private Submissao m_Submissao;

    public SubmissaoDecedidaState(Submissao mSubmissao) {
        this.m_Submissao = mSubmissao;
    }

    @Override
    public boolean valida() {
        return true;
    }

    @Override
    public SubmissaoCriadaState setSubmissaoCriadaState() {
        return null;
    }

    @Override
    public SubmissaoRegistadaState setRegistadaState() {

        return null;
    }

    @Override
    public SubmissaoRevistaState setSubmissaoRevistaState() {

        return null;
    }

    @Override
    public SubmissaoDistribuidaState setSubmissaoDistribuidaState() {

        return null;
    }

    @Override
    public SubmissaoDecedidaState setSubmissaoDecedidaState() {
        return null;
    }
}
