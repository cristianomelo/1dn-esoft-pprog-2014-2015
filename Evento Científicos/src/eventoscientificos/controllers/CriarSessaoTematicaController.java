/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import estados.EventoRegistadoState;
import estados.SessaoCriadaState;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Sessao;
import eventoscientificos.model.Utilizador;
import java.util.List;
import eventoscientificos.regista.registaEvento;
import eventoscientificos.regista.registaProponente;
import eventoscientificos.regista.registaSessao;
/**
 *
 * @author Marco/Cristiano
 */
public class CriarSessaoTematicaController {
    private Evento m_evento;
    private Empresa m_empresa;
    private Sessao m_sessao;
    private registaEvento r_evento;
    private SessaoCriadaState m_sessaoCriadaState;
    private registaSessao r_sessao;
    public CriarSessaoTematicaController(Empresa empresa)
    {
        m_empresa = empresa;

    }
     public List<Evento> getEventosOrganizador(String email)
    {
        return r_evento.getEventosOrganizador(email);
    }
    
    public void selectEvento(Evento e)
    {
        m_evento = e;
        r_sessao=m_evento.getRegistaSessao();
        m_sessao = r_sessao.novaSessao();
    }
   

    public String getSessaoString()
    {
        return m_sessao.toString();
    }

    public void setCodigo(String strCodigo)
    {
        m_sessao.setCodigo(strCodigo);
    }
    
    public void setDescricao(String strDescricao)
    {
        m_sessao.setDescricao(strDescricao);
    }

    public boolean addProponente(String email)
    {   
        registaProponente r_poponente=m_sessao.getRegistoProponente();
        Utilizador u = m_empresa.getUtilizador(email);
        if( u!=null)
            return r_poponente.addProponente( email, u );
        else
            return false;
    }
    
    public boolean registaSessao()
    {
        if(r_sessao.registaSessaoTematica(this.m_sessao)){
        m_sessao.getEstadoSessao().setRegistadaState();
        return true;
        }
        return false;
    }
     public void novaSessao()
    {
        r_sessao.novaSessao();
        m_sessaoCriadaState=new SessaoCriadaState(m_sessao);
        m_evento.getEstado().setSessoesDefenidasState();
    }

    public void createTimers() {
      m_evento.schedule(m_sessao,m_sessao.getDataFimSub());
    }
}
