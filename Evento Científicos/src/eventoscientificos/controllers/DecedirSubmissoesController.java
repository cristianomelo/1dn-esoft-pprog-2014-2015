/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import estados.EventoDistribuidoState;
import estados.SessaoEmDistribuicaoState;
import eventoscientificos.model.Decisivel;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.EstadosEvento;
import eventoscientificos.model.Evento;
import eventoscientificos.model.ProcessoDecisao;
import eventoscientificos.model.Sessao;
import eventoscientificos.model.Submissao;
import java.util.List;

/**
 *
 * @author Utilizador
 */
public class DecedirSubmissoesController {
    private Empresa m_empresa;
    private Decisivel m_decisivel;
    private ProcessoDecisao m_ProcessoDecisao;
    private String m_formula;
    private EstadosEvento m_estadosEvento;
    private Evento m_evento;
    private Sessao m_sessao;
    public DecedirSubmissoesController(Empresa m_empresa){
        this.m_empresa=m_empresa;
    }
    public List<Decisivel> getListaDecisiveis(String email) {
       return m_decisivel.getEventos_SessoesFaseDecisao(email);
    }

    public List<String> getFormulas() {
        return m_empresa.getFormulas();
    }

    public List<Submissao> avalia() {
      return m_ProcessoDecisao.avalia();
    }
    
    public void selectDecisivel(Decisivel dec) {
        this.m_decisivel=dec;
        if(m_decisivel instanceof Evento){
            m_evento=(Evento) dec;
        }else{
            m_sessao=(Sessao) dec;
        }
        m_ProcessoDecisao=m_decisivel.novaDecisao();
    }

    public void selectFormula(String formula) {
       m_ProcessoDecisao.setFormula(formula);
    }

    public boolean registaDecisao() {
        m_ProcessoDecisao.notifica(null);
        m_decisivel.setProcesso(m_ProcessoDecisao);
        if(m_evento!=null){
            m_evento.getEstado().setEventoDecedidoState();
        }else{
            m_sessao.getEstadoSessao().setDecedidoState();
        }
        return true;
    }
    
}
