/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.TipoConflitos;
import eventoscientificos.regista.RegistaTipoConflitos;

/**
 *
 * @author Utilizador
 */
public class CriarTipoConflitoController {
    private Empresa m_empresa;
    private RegistaTipoConflitos m_registaTipoConflitos;
    private TipoConflitos m_tipoConflito;
    
    public CriarTipoConflitoController(Empresa m_empresa){
        this.m_empresa=m_empresa;
    }
    
    public void getRegistaConflitos(){
        m_registaTipoConflitos=m_empresa.getRegistoConflito();
    }
    
    public void novoTipoConflito(){
        this.getRegistaConflitos();
        this.m_tipoConflito=m_registaTipoConflitos.novoTipoConflitos();
    }

    public void setDados() {
        m_tipoConflito.setDados();
    }

    public boolean registaTipoConflito() {
       return m_registaTipoConflitos.registaTipoConflito(m_tipoConflito);
    }

    public String getDados() {
        return m_tipoConflito.toString();
    }
}
