/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisao;
import eventoscientificos.regista.registaEvento;
import java.util.List;
import eventoscientificos.model.revisavel;
import eventoscientificos.regista.registaRevisao;

/**
 *
 * @author Marco/Cristiano
 */
public class ReverArtigoController 
{
    private Empresa m_empresa;
    private Revisao m_revisao;
    private Evento m_evento;
    private registaEvento r_evento;
    private registaRevisao r_revisao;
    public ReverArtigoController(Empresa m_empresa) {
        this.m_empresa = m_empresa;
    }

    public List<Evento> getEventosSessoesRevisor(String email) {
        return r_evento.getEventosSessoesRevisor(email);
    }
    
    public Artigo getArtigoRevisao(Revisao r){
        return r_revisao.getArtigoRevisao(r);
    }
    public List<Revisao> getRevisoesEvento_sessao(revisavel r){
        return r.getRevisoesEvento_sessao(r);
    }


    public Revisao setDados(String strAvaliacao, String strJustificacao) {
        m_revisao.setAvaliacao(strAvaliacao);
        m_revisao.setJustificacao(strJustificacao);
        return m_revisao;
    }

    public boolean registarRevisao() {
        return r_revisao.registarRevisao(m_revisao);
    }
}
