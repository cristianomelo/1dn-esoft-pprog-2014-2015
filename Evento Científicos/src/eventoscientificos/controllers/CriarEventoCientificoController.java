package eventoscientificos.controllers;

import estados.EventoCriadoState;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Utilizador;
import eventoscientificos.regista.registaEvento;
import eventoscientificos.regista.registaOrganizador;
import java.util.Timer;
/**
 *
 * @author Marco/Cristiano
 */

public class CriarEventoCientificoController
{
    private Empresa m_empresa;
    private Evento m_evento;
    private registaEvento r_evento;
    private registaOrganizador r_organizador;
    private EventoCriadoState m_estadoCriado;
    public CriarEventoCientificoController(Empresa empresa)
    {
        m_empresa = empresa;
        r_evento=m_empresa.getRegistoEvento();
    }

    public void novoEvento()
    {   
        m_evento = r_evento.novoEvento();
        m_estadoCriado=new EventoCriadoState(m_evento);
    }
    
    public String getEventoString()
    {
        return m_evento.toString();
    }

    public void setTitulo(String strTitulo)
    {
        m_evento.setTitulo(strTitulo);
    }
    
    public void setDescricao(String strDescricao)
    {
        m_evento.setDescricao(strDescricao);
    }

    public void setLocal(String strLocal)
    {
        m_evento.setLocal(strLocal);
    }
    
    public void setDataInicio(String strDataInicio)
    {
        m_evento.setDataInicio(strDataInicio);
    }

    public void setDataFim(String strDataFim)
    {
        m_evento.setDataFim(strDataFim);
    }
    
    public boolean addOrganizador(String strId)
    {   
        r_organizador=m_evento.getRegistoOrganizador();
        Utilizador u = m_empresa.getUtilizador(strId);
        
        if( u!=null)
            return r_organizador.addOrganizador( strId, u );
        else
            return false;
    }
    
    public boolean registaEvento()
    {   
        if(r_evento.registaEvento(m_evento)){
          m_estadoCriado.setRegistadoState();
           return true;
         }
         return false;
    }

    public void createTimers() {
        m_empresa.schedule(m_evento,m_evento.getDataFimSub());
    }

}

