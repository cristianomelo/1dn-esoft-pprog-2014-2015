/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Sessao;
import eventoscientificos.model.Mecanismo;
import eventoscientificos.model.ProcessoDistribuicao;
import java.util.List;
import eventoscientificos.regista.registaEvento;
import eventoscientificos.regista.registaMecanismo;

/**
 *
 * @author Marco/Cristiano
 */
public class DistribuirRevisoesController {
    private Empresa m_empresa;
    private ProcessoDistribuicao pd;
    private registaEvento r_evento;
    private registaMecanismo r_mecMecanismo;
    public DistribuirRevisoesController(Empresa empresa) {
        this.m_empresa = empresa;
    }

    public List<Evento> getEventosOrgProp(String strIdOrgProp) {
        return r_evento.getEventosOrganizador(strIdOrgProp);
    }

    public void selectEvento(Evento e) {
        pd= e.novoProcessoDistribuicao();
    }

    public void selectSessaoTematica(Sessao st) {
        pd=st.novoProcessoDistribuicao();
    }

    public List<Mecanismo> getMecanismoDistruicao() {
        return r_mecMecanismo.getListaMecanismos();
    }

    public void setMecanismoDistribuicao(Mecanismo m) {
        pd.setMecanismoDistribuicao(m);
        pd.distribui(null);
    }
    
    public void registaDistribuicao(Evento e) {
        e.setProcessoDistribuicao(pd);
        e.getEstado().valida();
    }
    public void registaDistribuicao(Sessao st) {
        st.setProcessoDistribuicao(pd);
        st.getEstadoSessao().valida();
    }
}
