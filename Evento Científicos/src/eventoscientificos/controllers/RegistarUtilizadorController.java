package eventoscientificos.controllers;

import eventoscientificos.model.Utilizador;
import eventoscientificos.model.Empresa;
import eventoscientificos.regista.registaUtilizador;
/**
 *
 * @author Marco/Cristiano
 */

public class RegistarUtilizadorController
{
    private Empresa m_empresa;
    private Utilizador m_utilizador;
    private registaUtilizador r_utilizador;
    public RegistarUtilizadorController(Empresa empresa)
    {
        m_empresa = empresa;
        r_utilizador=m_empresa.registaUtilizador();
    }

    public void novoUtilizador()
    {
        m_utilizador = r_utilizador.novoUtilizador();
    }

    public Utilizador setDados(String strUsername, String strPassword, String strNome, String strEmail)
    {
        m_utilizador.setUsername(strUsername);
        m_utilizador.setPassword(strPassword);
        m_utilizador.setNome(strNome);
        m_utilizador.setEmail(strEmail);
        
        if(r_utilizador.registaUtilizador(m_utilizador) )
            return m_utilizador;
        else
            return null;
    }
}

