package eventoscientificos.controllers;

import eventoscientificos.model.CP;
import eventoscientificos.model.Utilizador;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.EstadosEvento;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Sessao;
import java.util.List;
import eventoscientificos.regista.registaEvento;
import eventoscientificos.regista.registaRevisor;

/**
 *
 * @author Marco/Cristiano
 */

public class CriarCPdeSTController
{
    private Empresa m_empresa;
    private Evento m_evento;
    private Sessao m_sessao;
    private CP m_cp;
    private registaEvento r_evento;
    private EstadosEvento estadosEvento;
    private registaRevisor r_revisor;
    public CriarCPdeSTController(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public List<Evento> getEventosOrganizador(String strId)
    {
        return r_evento.getEventosOrganizador(strId);
    }
    
    public void selectEvento(Evento e)
    {
        m_evento = e;

    }
    public void  selectSessao(Sessao st){
        m_sessao=st;
        m_cp=m_sessao.novaCP();
    }
    public Revisor addMembroCP(String stremail)
    {   
        r_revisor=m_cp.getRegistoRevisor();
        Utilizador u = m_empresa.getUtilizador(stremail);
        
        if( u!=null)
            return r_revisor.addMembroCP( stremail, u );
        else
            return null;
    }
    
    public boolean registaMembroCP( Revisor r )
    {
        return r_revisor.registaMembroCP(r);
    }
    
    public void setCP()
    {
        m_sessao.setCP(m_cp);
        m_sessao.getEstadoSessao().setCPDefenidaState();
    }
}

