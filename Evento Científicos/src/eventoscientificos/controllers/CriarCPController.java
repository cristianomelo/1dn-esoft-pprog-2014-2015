package eventoscientificos.controllers;

import eventoscientificos.model.CP;
import eventoscientificos.model.Utilizador;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisor;
import java.util.List;
import eventoscientificos.regista.registaEvento;
import eventoscientificos.regista.registaRevisor;

/**
 *
 * @author Marco/Cristiano
 */

public class CriarCPController
{
    private Empresa m_empresa;
    private Evento m_evento;
    private CP m_cp;
    private registaEvento r_evento;
    private registaRevisor r_revisor;

    public CriarCPController(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public List<Evento> getEventosOrganizador(String strId)
    {
        return r_evento.getEventosOrganizador(strId);
    }
    
    public void selectEvento(Evento e)
    {
        m_evento = e;
        m_cp = m_evento.novaCP();
    }
    
    public Revisor addMembroCP(String strId)
    {
        r_revisor=m_cp.getRegistoRevisor();
        Utilizador u = m_empresa.getUtilizador(strId);
        
        if( u!=null)
            return r_revisor.addMembroCP( strId, u );
        else
            return null;
    }
    
    public boolean registaMembroCP( Revisor r )
    {
        return r_revisor.registaMembroCP(r);
    }
    
    public void setCP()
    {
        m_evento.setCP(m_cp); 
        m_evento.getEstado().setCPDefenidaState();
    }
}

