/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import estados.SubmissaoCriadaState;
import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.EstadosSubmissao;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Sessao;
import eventoscientificos.model.Submissao;
import java.util.List;
import eventoscientificos.regista.registaSubmissao;
import eventoscientificos.regista.registoAutor;
import eventoscientificos.regista.registaEvento;
/**
 *
 * @author Marco/Cristiano
 */
public class SubmeterArtigoController {

    private Empresa m_empresa;
    private Evento m_evento;
    private Submissao m_submissao;
    private EstadosSubmissao m_state;
    private Artigo m_artigo;
    private Sessao m_sessao;
    private EstadosSubmissao m_SubmissaoCriadaState;
    private registoAutor r_autor;
    private registaEvento r_evento;
    private registaSubmissao r_submissao;

    public SubmeterArtigoController(Empresa empresa) {
        m_empresa = empresa;
    }

    public List<Evento> iniciarSubmissao() {
        return this.r_evento.getListaEventosPodeSubmeter();
    }

    public void selectEvento(Evento e) {
        m_evento = e;
        r_submissao=e.getRegistoSubmissao();
        this.m_submissao = r_submissao.novaSubmissao();
        this.m_artigo = this.m_submissao.novoArtigo();
    }

    public void selectSessao(Sessao s) {
        m_sessao = s;
        r_submissao=s.getRegistoSubmissao();
        this.m_submissao = r_submissao.novaSubmissao();
        m_SubmissaoCriadaState=new SubmissaoCriadaState(m_submissao);
        this.m_artigo = this.m_submissao.novoArtigo();
    }

    public void setDados(String strTitulo, String strResumo) {
        this.m_artigo.setTitulo(strTitulo);
        this.m_artigo.setResumo(strResumo);
    }

    public Autor novoAutor(String strNome, String strAfiliacao) {
        r_autor=m_artigo.getRegistoAutor();
        return this.r_autor.novoAutor(strNome, strAfiliacao);
    }

    public boolean addAutor(Autor autor) {
        return this.r_autor.addAutor(autor);
    }

    public List<Autor> getPossiveisAutoresCorrespondentes() {
        return this.r_autor.getPossiveisAutoresCorrespondentes();
    }

    public void setCorrespondente(Autor autor) {
        this.m_artigo.setAutorCorrespondente(autor);
    }

    public void setFicheiro(String strFicheiro) {
        this.m_artigo.setFicheiro(strFicheiro);
    }

    public String getInfoResumo() {
        return this.m_submissao.getInfo() + this.m_artigo.getInfo();
    }

    public boolean registarSubmissao() {
        this.m_submissao.setArtigo(m_artigo);
        m_SubmissaoCriadaState.setRegistadaState();
        return r_submissao.addSubmissao(m_submissao);
    }
}
