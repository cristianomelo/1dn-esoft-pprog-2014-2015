/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import java.util.List;
import eventoscientificos.regista.registaSubmissao;
import eventoscientificos.regista.registoAutor;

/**
 *
 * @author Utilizador
 */
public class alteraSubmissao_Controller {

    private registaSubmissao r_submissao;
    private registoAutor r_autor;
    private Submissao sub;
    private Artigo m_artigo;

    public alteraSubmissao_Controller() {

    }

    public List<Submissao> iniciarAlteracaoSubmissao(String email) {
        return this.r_submissao.getSubmissoesAutor(email);
    }

    public Autor getAutor(String email) {
        return r_autor.get(email);
    }

    public List<Autor> getAutoresArtigo(Artigo a) {
        return r_autor.getAutoresArtigo(a);
    }

    public int procura(Autor a) {
        return r_autor.indexOf(a);
    }

    public void removeAutor(Autor aut) {
        r_autor.remove(aut);
    }

    public boolean addAutor(Autor autor) {
        return this.r_autor.addAutor(autor);
    }

    public Artigo getArtigoSubmissao() {
        m_artigo = r_submissao.getArtigoSubmissao(sub);
        return m_artigo;
    }

    public void selectSubmissao(Submissao s) {
        sub = s;

        this.m_artigo = r_submissao.getArtigoSubmissao(s);
    }

    public boolean registarAlteracao(Submissao s) {
        this.sub.setArtigo(m_artigo);
        return r_submissao.registarAlteracao(sub,s);
    }

    public void setDados(String strTitulo, String strResumo) {
        this.m_artigo.setTitulo(strTitulo);
        this.m_artigo.setResumo(strResumo);
    }

    public String getInfoResumo() {
        return this.sub.getInfo() + this.m_artigo.getInfo();
    }

    public void setFicheiro(String strFicheiro) {
        this.m_artigo.setFicheiro(strFicheiro);
    }

    public void setCorrespondente(Autor autor) {
        this.m_artigo.setAutorCorrespondente(autor);
    }

    public List<Autor> getPossiveisAutoresCorrespondentes() {
        return this.r_autor.getPossiveisAutoresCorrespondentes();
    }

    public Autor novoAutor(String strNome, String strAfiliacao) {
        r_autor = m_artigo.getRegistoAutor();
        return this.r_autor.novoAutor(strNome, strAfiliacao);
    }

    public Autor editaAutor(String strNome, String strAfiliacao) {
        return this.r_autor.alteraAutor(strNome, strAfiliacao);
    }

}
