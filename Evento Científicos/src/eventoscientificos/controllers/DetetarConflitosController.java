/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.model.Detetavel;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Sessao;

/**
 *
 * @author Utilizador
 */
public class DetetarConflitosController {
    private Empresa m_Empresa;
    private Evento m_Evento;
    private Sessao m_Sessao;
    public DetetarConflitosController(Empresa m_empresa){
        this.m_Empresa=m_empresa;
    }
    public void iniciaDetecaoConflitos(Detetavel  det){
        det.detetarConflito();
        if(det instanceof Evento){
            m_Evento=(Evento) det;
            m_Evento.getEstado().valida();
        }else{
            m_Sessao=(Sessao) det;
            m_Sessao.getEstadoSessao().valida();
        }
    }
}
