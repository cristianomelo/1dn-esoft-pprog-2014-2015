/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.CriarCPController;
import eventoscientificos.controllers.CriarCPdeSTController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Sessao;
import java.util.ArrayList;
import java.util.List;
import utils.Utils;
import eventoscientificos.regista.registaSessoes;

/**
 *
 * @author Marco/Cristiano
 */
public class CriarCPSessaoUI {
   

    private Empresa m_empresa;
    private CriarCPdeSTController m_controllerCPST;
    private registaSessoes r_sessao;
    public CriarCPSessaoUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controllerCPST = new CriarCPdeSTController(m_empresa);
    }

    public void run()
    {
        String strIdProponente = introduzIdProponente();
        
        List<Sessao> leST = new ArrayList<Sessao>();
        List<Evento> le = m_controllerCPST.getEventosOrganizador(strIdProponente);
            
        
        apresentaEventosProponente(strIdProponente, le);
        
        Evento e = selecionaEvento(le);
        leST=r_sessao.getListaSessoesTematicasEvento(e);
        apresentaSessoesEvento(strIdProponente,leST);
        Sessao st=selecionaSessao(leST);
        
        if (e != null)
        {
            m_controllerCPST.selectEvento(e);
            m_controllerCPST.selectSessao(st);
        
            adicionaMembrosCP();
        
            m_controllerCPST.setCP();
        
            System.out.println("Terminado.");
        }
        else
            System.out.println("Criação da Comissão de Programa cancelada.");
    }

    private String introduzIdProponente()
    {
        return Utils.readLineFromConsole("Introduza Id Proponente: ");
    }
   
    private void apresentaEventosProponente(String strIdOrganizador, List<Evento> le)
    {
        System.out.println("Eventos do proponente " + strIdOrganizador + ":");
        
        int index = 0;
        for(Evento e : le)
        {
            index++;

            System.out.println(index + ". " + e.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }
    
    private void apresentaSessoesEvento(String strIdOrganizador, List<Sessao> ls)
    {
        System.out.println("Eventos do proponente " + strIdOrganizador + ":");
        
        int index = 0;
        for(Sessao s : ls)
        {
            index++;

            System.out.println(index + ". " + s.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private Evento selecionaEvento(List<Evento> le)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > le.size());

        if( nOpcao == 0 )
            return null;
        else
            return le.get(nOpcao - 1);
    }
    
    
    private Sessao selecionaSessao(List<Sessao> ls)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > ls.size());

        if( nOpcao == 0 )
            return null;
        else
            return ls.get(nOpcao - 1);
    }
    
    private void adicionaMembrosCP()
    {
        String strResposta;
        do
        {
            String strId = Utils.readLineFromConsole("Introduza Id do Membro: ");

            Revisor r = m_controllerCPST.addMembroCP(strId);

            if( r ==null )
                System.out.println("Membro não criado.");
            else
            {
                System.out.println("Membro CP: " + r.toString() );
                           
                if( confirmaNovoMembroCP() )
                {
                    if( m_controllerCPST.registaMembroCP( r ) )
                        System.out.println("Membro da CP adicionado.");
                    else 
                        System.out.println("Membro da CP não adicionado.");
                }
            }
            
            strResposta = Utils.readLineFromConsole("Introduzir outro membro CP (S/N)? ");
        }
        while (strResposta.equalsIgnoreCase("S"));
    }
    
    private boolean confirmaNovoMembroCP()
    {
        String strConfirma;
        do
        {
            strConfirma = Utils.readLineFromConsole("Confirma novo membro da CP (S/N): ");
        }
        while ( !strConfirma.equalsIgnoreCase("s") && !strConfirma.equalsIgnoreCase("n") );

        if( strConfirma.equalsIgnoreCase("s") )
            return true;
        else
            return false;
    }  

}
