/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.DecedirSubmissoesController;
import eventoscientificos.model.Decisivel;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Utilizador
 */
public class DecedirSubmissoesUI {
    private DecedirSubmissoesController m_controllerDecedirSubmissoe;
    
    public DecedirSubmissoesUI(Empresa m_empresa){
        m_controllerDecedirSubmissoe=new DecedirSubmissoesController(m_empresa);
    }
    public void run(){
        boolean decedido=false;
        String email=utils.Utils.readLineFromConsole("Email do Organizador/Proponente:");
        List<Decisivel> listaDecisiveis=m_controllerDecedirSubmissoe.getListaDecisiveis(email);
        apresentaListaEventos_Sessoes(listaDecisiveis);
        Decisivel dec=selecionaDecisivel(listaDecisiveis);
        m_controllerDecedirSubmissoe.selectDecisivel(dec);
        List<String> listaFormulas=m_controllerDecedirSubmissoe.getFormulas();
        while(! decedido){
            apresentaFormulas(listaFormulas);
            String formula=selectFormula(listaFormulas);
            m_controllerDecedirSubmissoe.selectFormula(formula);
            List<Submissao> listaSubmissoesAvalia=m_controllerDecedirSubmissoe.avalia();
            apresentaListadeSubmissoes(listaSubmissoesAvalia);
            if(confirmaLista()){
                decedido=true;
            }
        }
        if(m_controllerDecedirSubmissoe.registaDecisao()){
            System.out.println("decisao registada com sucesso");
        }else{
             System.out.println("decisao registada sem sucesso");
    }
    }
    public void apresentaListaEventos_Sessoes(List<Decisivel> listaDecisiveis){
       for(Decisivel dec:listaDecisiveis){
           System.out.println("");
       }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }
    private Decisivel selecionaDecisivel(List<Decisivel> ld)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > ld.size());

        if( nOpcao == 0 )
            return null;
        else
            return ld.get(nOpcao - 1);
    }

    private void apresentaFormulas(List<String> listaFormulas) {
        for(String st:listaFormulas){
           System.out.println("");
       }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private String selectFormula(List<String> listaFormulas) {
       String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > listaFormulas.size());

        if( nOpcao == 0 )
            return null;
        else
            return listaFormulas.get(nOpcao - 1);
    }

    private void apresentaListadeSubmissoes(List<Submissao> listaSubmissoesAvalia) {
         for(Submissao st:listaSubmissoesAvalia){
           System.out.println("");
       }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }
    public boolean confirmaLista(){
        String confirma=utils.Utils.readLineFromConsole("Confirma lista de submissoes?");
        if(confirma.equals("sim")){
            return true;
        }
        return false;
    }
}
