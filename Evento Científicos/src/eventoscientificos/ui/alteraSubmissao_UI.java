/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Submissao;
import eventoscientificos.regista.registaSubmissao;
import eventoscientificos.controllers.alteraSubmissao_Controller;
import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Sessao;
import eventoscientificos.regista.registoAutor;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Utilizador
 */
public class alteraSubmissao_UI {

    private alteraSubmissao_Controller m_controllerAS;
    private registaSubmissao r_submissao;
    private Artigo m_artigo;
    private registoAutor r_autor;

    public alteraSubmissao_UI() {

    }

    public void run() {
        String email = Utils.readLineFromConsole("ID autor");
        List<Submissao> ls = m_controllerAS.iniciarAlteracaoSubmissao(email);
        apresentaSubmissoes(ls);
        Submissao s = selecionaSubmissao(ls);
        m_controllerAS.selectSubmissao(s);
        apresentadadosSubmissao(s);
        String resposta;
        resposta = Utils.readLineFromConsole("Pretende Alterar os dados da submissão?");
        if (resposta.equals("sim")) {
            String strTitulo = Utils.readLineFromConsole("Introduza o Título do Artigo: ");

            String strResumo = Utils.readLineFromConsole("Introduza o Resumo do Artigo: ");

            m_controllerAS.setDados(strTitulo, strResumo);
        }
        resposta = Utils.readLineFromConsole("Pretende Alterar os autores do artigo?");
        if (resposta.equals("sim")) {
            System.out.println("Opção:");
            System.out.println("1-Remover Autor:");
            System.out.println("2-Editar Autor:");
            System.out.println("3-Adicionar Autor:");
            resposta = Utils.readLineFromConsole("Qual a opção?");
            int op = Integer.parseInt(resposta);
            switch (op) {
                case 1:
                    removeautores();
                    break;
                case 2:
                    editarAutores();
                    break;
                case 3:
                    adicionarAutores();
                    break;

                default:
                    System.out.println("Erro");
            }

            List<Autor> la = m_controllerAS.getPossiveisAutoresCorrespondentes();

            listaAutores(la);

            Autor autor = selecionaAutor(la);

            m_controllerAS.setCorrespondente(autor);
        }
        resposta = Utils.readLineFromConsole("Pretende alterar o ficheiro");
        if (resposta.equals("sim")) {
            String strFicheiro = Utils.readLineFromConsole("Introduza o Ficheiro do Artigo: ");
            m_controllerAS.setFicheiro(strFicheiro);

            String strQuestao = "Confirma a submissão do artigo com a seguinte informação: \n" + m_controllerAS.getInfoResumo() + "\n Opção (S/N):";
            boolean bConfirma = confirma(strQuestao);

            if (bConfirma) {
                if (m_controllerAS.registarAlteracao(s)) {
                    System.out.println("Submissão concluida com sucesso.");
                } else {
                    System.out.println("Submissão cancelada devido a erros.");
                }

            } else {
                System.out.println("Submissão de artigo cancelada.");
            }

            System.out.println("Terminado.");
        } else {
            System.out.println("Submissão de artigo cancelada.");

        }
    }

    private Submissao selecionaSubmissao(List<Submissao> ls) {
        String opcao;
        int nOpcao;
        do {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > ls.size());

        if (nOpcao == 0) {
            return null;
        } else {
            return ls.get(nOpcao - 1);
        }
    }

    private void apresentaSubmissoes(List<Submissao> ls) {
        System.out.println("Submissao: ");

        int index = 0;
        for (Submissao s : ls) {
            index++;

            System.out.println(index + ". " + s.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    public void apresentadadosSubmissao(Submissao s) {
        m_artigo = r_submissao.getArtigoSubmissao(s);
    }

    private void adicionarAutores() {
        String strResposta;
        do {
            String strNome = Utils.readLineFromConsole("Introduza Nome do Autor: ");
            String strAfiliacao = Utils.readLineFromConsole("Introduza Afiliação do Autor: ");

            Autor autor = m_controllerAS.novoAutor(strNome, strAfiliacao);

            String strQuestao = "Confirma os dados do Autor: \n" + autor.toString() + "\n Opção (S/N):";
            boolean bConfirma = confirma(strQuestao);

            if (bConfirma) {
                if (m_controllerAS.addAutor(autor)) {
                    System.out.println("Autor adicionado.");
                } else {
                    System.out.println("Autor não adicionado.");
                }
            } else {
                System.out.println("Autor não adicionado.");
            }

            strResposta = Utils.readLineFromConsole("Introduzir outro autor (S/N)? ");
        } while (strResposta.equalsIgnoreCase("S"));
    }

    public void removeautores() {
        List<Autor> lista_Autor = m_controllerAS.getAutoresArtigo(m_artigo);
        listaAutores(lista_Autor);
        String resposta = Utils.readLineFromConsole("Id do autor que pretende remover");
        Autor aut = m_controllerAS.getAutor(resposta);
        if (m_controllerAS.procura(aut) != -1) {
            m_controllerAS.removeAutor(aut);
        }
    }

    public void listaAutores(List<Autor> lista_autores) {

    }

    public void editarAutores() {
        List<Autor> lista_Autor = m_controllerAS.getAutoresArtigo(m_artigo);
        listaAutores(lista_Autor);
        String resposta = Utils.readLineFromConsole("Id do autor a alterar");
        Autor aut = m_controllerAS.getAutor(resposta);
        if (m_controllerAS.procura(aut) != -1) {

            String strNome = Utils.readLineFromConsole("Introduza Nome do Autor: ");
            String strAfiliacao = Utils.readLineFromConsole("Introduza Afiliação do Autor: ");
        }

    }

    private boolean confirma(String questao) {
        String strConfirma;
        do {
            strConfirma = Utils.readLineFromConsole(questao);
        } while (!strConfirma.equalsIgnoreCase("s") && !strConfirma.equalsIgnoreCase("n"));

        return strConfirma.equalsIgnoreCase("s");
    }

    private Autor selecionaAutor(List<Autor> la) {
        String opcao;
        int nOpcao;
        do {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > la.size());

        if (nOpcao == 0) {
            return null;
        } else {
            return la.get(nOpcao - 1);
        }
    }
}
