/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.CriarEventoCientificoController;
import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.model.Empresa;
import eventoscientificos.regista.registaEvento;
import eventoscientificos.model.Evento;
import java.util.ArrayList;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Marco/Cristiano
 */
public class CriarSessaoUI {
   
    private Empresa empresa;
    private registaEvento r_evento;
    private CriarSessaoTematicaController m_controllerCST;
    List<Evento> lEvento=new ArrayList<Evento>();

    public CriarSessaoUI( Empresa empresa )
    {
        empresa = empresa;
        m_controllerCST = new CriarSessaoTematicaController(empresa);
    }

    public void run()
    {
        String strEmail = introduzIdOrganizador();
        
        List<Evento> le = m_controllerCST.getEventosOrganizador(strEmail);
        
        apresentaEventosOrganizador(strEmail, le);
        
        Evento e = selecionaEvento(le);
        novoSessao();
        r_evento.getEventosOrganizador(strEmail);
        selecionaEvento(le);
        introduzCodigo();
        introduzDescricao();
        adicionaProponentes();
        if (e != null){
            introduzDescricao();
            introduzCodigo();
            adicionaProponentes();
            apresentaSessao();
        }

        
        if( confirmaCriacaoSessao() )
        {
            if( m_controllerCST.registaSessao() ){
                System.out.println("Sessão registado.");
            m_controllerCST.createTimers();
            }else{ 
                System.out.println("Sessão não registado.");
            }
        }
    }
     private void novoSessao()
    {
        m_controllerCST.novaSessao();
    }

    private void introduzCodigo()
    {
        String strCodigo = Utils.readLineFromConsole("Introduza Codigo: ");

        m_controllerCST.setCodigo(strCodigo);
    }

    private void introduzDescricao()
    {
        String strDescricao = Utils.readLineFromConsole("Introduza Descrição: ");

        m_controllerCST.setDescricao(strDescricao);
    }
     private void adicionaProponentes()
    {
        String strResposta;
        do
        {
            String strId = Utils.readLineFromConsole("Introduza email do Proponente: ");

            boolean bAdicionado = m_controllerCST.addProponente(strId);

            if( bAdicionado )
                System.out.println("Proponentes adicionado.");
            else
                System.out.println("Proponentes não adicionado.");
            
            strResposta = Utils.readLineFromConsole("Introduzir outro Proponentes (S/N)? ");
        }
        while (strResposta.equalsIgnoreCase("S"));
    }
     private void apresentaEventosOrganizador(String strIdOrganizador, List<Evento> le)
    {
        System.out.println("Eventos do organizador " + strIdOrganizador + ":");
        
        int index = 0;
        for(Evento e : le)
        {
            index++;

            System.out.println(index + ". " + e.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }
    private void apresentaSessao()
    {
        System.out.println("Sessão Temática: ");
     
        System.out.println( m_controllerCST.getSessaoString());
    }
    
    private boolean confirmaCriacaoSessao()
    {
        String strConfirma;
        do
        {
            strConfirma = Utils.readLineFromConsole("Confirma criação de sessão (S/N): ");
        }
        while ( !strConfirma.equalsIgnoreCase("s") && !strConfirma.equalsIgnoreCase("n") );

        if( strConfirma.equalsIgnoreCase("s") )
            return true;
        else
            return false;
    }
      private String introduzIdOrganizador()
    {
        return Utils.readLineFromConsole("Introduza Id Organizador: ");
    }
private Evento selecionaEvento(List<Evento> le)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > le.size());

        if( nOpcao == 0 )
            return null;
        else
            return le.get(nOpcao - 1);
    }
}
