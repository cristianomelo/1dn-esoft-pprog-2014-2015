/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import utils.Utils;
import eventoscientificos.controllers.Licitacao_Controller;
import eventoscientificos.model.Artigo;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Utilizador;
import java.util.List;
import eventoscientificos.model.Licitacao;
import eventoscientificos.model.Licitavel;

/**
 *
 * @author Utilizador
 */
public class LicitacaoUI {

    private Licitacao_Controller m_controllerL;
    private Empresa m_empresa;
    private Licitacao l;

    public LicitacaoUI(Empresa empresa) {
        m_empresa = empresa;
    }

    public void run() {
        String email = Utils.readLineFromConsole("Id revisor?");
        List<Licitavel> listaLicitaveis=m_controllerL.novaLicitacao(email);
        apresentaListadeLicitaveis(listaLicitaveis);
        Licitavel li=selectLicitavel(listaLicitaveis);
        m_controllerL.selectLicitavel(li);
        List<Artigo> la = m_controllerL.getArtigosLicitavel();
        apresentaArtigosRevisor(la, email);
        Artigo a = selecionaArtigo(la);
        m_controllerL.selectArtigo(a,email);
        l = introduzDadosLicitação();
        apresentaDadosLicitacao(l);
    }

    private Licitacao introduzDadosLicitação() {
        String strgrauIntresse = Utils.readLineFromConsole("Introduza Grau de Intress: ");

        String strConflitosIntresse = Utils.readLineFromConsole("Introduza conflitos de intresse: ");

        return m_controllerL.setDados(strgrauIntresse, strgrauIntresse);
    }


    public void apresentaArtigosRevisor(List<Artigo> la, String email) {
        System.out.println("Artigos do Revisor " + email + ":");

        int index = 0;
        for (Artigo a : la) {
            index++;

            System.out.println(index + ". " + a.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private Artigo selecionaArtigo(List<Artigo> la) {
        String opcao;
        int nOpcao;
        do {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > la.size());

        if (nOpcao == 0) {
            return null;
        } else {
            return la.get(nOpcao - 1);
        }
    }

    private void apresentaDadosLicitacao(Licitacao l) {
        if (l == null) {
            System.out.println("Utilizador não registado.");
        } else {
            System.out.println(l.toString());
        }
    }
        private Licitavel selectLicitavel(List<Licitavel> listaLicitavel) {
       String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > listaLicitavel.size());

        if( nOpcao == 0 )
            return null;
        else
            return listaLicitavel.get(nOpcao - 1);
    }

    private void apresentaListadeLicitaveis(List<Licitavel> listaLicitaveis) {
         for(Licitavel li:listaLicitaveis){
           System.out.println("");
       }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }
}
