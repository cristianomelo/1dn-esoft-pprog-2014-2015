/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Empresa;
import eventoscientificos.controllers.CriarTipoConflitoController;

/**
 *
 * @author Utilizador
 */
public class CriarTipoConflitosUI {
    private Empresa m_empresa;
    private CriarTipoConflitoController m_criarTipoConflitoController;
    
    public CriarTipoConflitosUI(Empresa m_empresa){
        this.m_empresa=m_empresa;
        m_criarTipoConflitoController=new CriarTipoConflitoController(m_empresa);
    }
    public void run(){
       m_criarTipoConflitoController.novoTipoConflito();
       lerDados();
       if(mostraDados()){
           if(registaTipoConflito()){
               System.out.println("Tipo Conflito adicionado");
           }else{
               System.out.println("Tipo de COnflito não adicionado");
           }
       }
    }
    
    public void lerDados(){
        m_criarTipoConflitoController.setDados();
    }
    public boolean registaTipoConflito(){
        boolean resultado;
        resultado=m_criarTipoConflitoController.registaTipoConflito();
        return resultado;
    }
    
    public boolean mostraDados(){
     System.out.println(""+ m_criarTipoConflitoController.getDados());
     String confirma=utils.Utils.readLineFromConsole("Confirma dados?");
     if(confirma=="Sim"){
         return true;
     }
     return false;
    }
}
