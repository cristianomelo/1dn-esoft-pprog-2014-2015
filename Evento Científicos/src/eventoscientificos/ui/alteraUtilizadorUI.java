/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.alterarUtilizador_Controller;
import eventoscientificos.model.Utilizador;
import utils.Utils;

/**
 *
 * @author Utilizador
 */
public class alteraUtilizadorUI {
    private Utilizador u;
    private alterarUtilizador_Controller m_controllerAU;
    public alteraUtilizadorUI(){
        
    }
            
    public void run()
    {
        String id=utils.Utils.readLineFromConsole("ID do utilizador");
        u = m_controllerAU.getUtilizador(id);
        apresentaDadosUtilizador(u);
        u=insereDadosAlterados();
        apresentaDadosUtilizador( u );
    }
       private Utilizador insereDadosAlterados()
    {
        String strUsername = Utils.readLineFromConsole("Introduza Username: ");

        String strPassword = Utils.readLineFromConsole("Introduza Password: ");

        String strNome = Utils.readLineFromConsole("Introduza Nome: ");

        String strEmail = Utils.readLineFromConsole("Introduza Email: ");

        return m_controllerAU.setDadosAlterados(strUsername, strPassword, strNome, strEmail);
    }
       
      private void apresentaDadosUtilizador( Utilizador utilizador )
    {
        if(utilizador == null)
            System.out.println("Utilizador não registado.");
        else
            System.out.println(utilizador.toString() );
    }
}
