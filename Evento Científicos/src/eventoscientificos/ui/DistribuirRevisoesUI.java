/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Mecanismo;
import eventoscientificos.controllers.DistribuirRevisoesController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Sessao;
import eventoscientificos.regista.registaSessao;
import java.util.List;
import utils.Utils;

/**
 *
 * @author Marco/Cristiano
 */
public class DistribuirRevisoesUI
{
    private Empresa m_empresa;
    private DistribuirRevisoesController m_distController;
    private registaSessao r_sessao;
    public DistribuirRevisoesUI(Empresa empresa) {
        m_empresa = empresa;
        m_distController = new DistribuirRevisoesController(empresa);
    }
    public void run()
    {
        String strIdOrgProp = introduzIdOrg_Prop();
        
        List<Evento> le = m_distController.getEventosOrgProp(strIdOrgProp);
        
        apresentaEventosOrganizador(strIdOrgProp, le);
        
        Evento e = selecionaEvento(le);
        
        apresentaSessoesTematicas(e);
        
        Sessao st = selecionaSessaoTematica(e);
        
        if (e != null)
        {
            if(st != null)
                m_distController.selectEvento(e);
            else
                m_distController.selectSessaoTematica(st);
            
            List<Mecanismo> mc = m_distController.getMecanismoDistruicao();
            
            do
            {
                apresentaMecanismos(mc);

                Mecanismo m = selecionaMecanismo();
                m_distController.setMecanismoDistribuicao(m);
                
                apresentarResultadoDistribuicao();
                
            } while(!Utils.readLineFromConsole("Distribuição definitiva[S/N]: ").toUpperCase().contentEquals("S"));
            
            m_distController.registaDistribuicao(st);
        }
        else
            System.out.println("A distribuição de Revisões foi cancelada.");
    }
    
    private String introduzIdOrg_Prop()
    {
        return Utils.readLineFromConsole("Introduza Id Organizador/Proponente: ");
    }
    
      private void apresentaEventosOrganizador(String strIdOrganizador, List<Evento> le)
    {
        System.out.println("Eventos do organizador/proponente " + strIdOrganizador + ":");
        
        int index = 0;
        for(Evento e : le)
        {
            index++;

            System.out.println(index + ". " + e.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private Evento selecionaEvento(List<Evento> le)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > le.size());

        if( nOpcao == 0 )
            return null;
        else
            return le.get(nOpcao - 1);
    }
    
    private Sessao selecionaSessaoTematica(Evento e)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > r_sessao.getSessoesEvento(e).size());

        if( nOpcao == 0 )
            return null;
        else
            return r_sessao.getSessoesEvento(e).get(nOpcao - 1);
    }

    private void apresentaSessoesTematicas(Evento e) {
       System.out.println("Sessoes tematicas do evento " + e.getTitulo() + ":");
        
        int index = 0;
        for(Sessao st : r_sessao.getSessoesEvento(e))
        {
            index++;

            System.out.println(index + ". " + st.toString   ());
        }
        System.out.println("");
        System.out.println("0 - Evento Principal");}

    private void apresentaMecanismos(List<Mecanismo> mc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Mecanismo selecionaMecanismo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void apresentarResultadoDistribuicao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
