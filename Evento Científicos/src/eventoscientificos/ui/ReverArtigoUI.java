/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.ReverArtigoController;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Artigo;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.revisavel;
import java.util.List;
import utils.Utils;
/**
 *
 * @author Marco/Cristiano
 */
public class ReverArtigoUI
{
    private ReverArtigoController m_controllerRA;
    private Empresa m_empresa;
    private Artigo m_artigo;
    
    public ReverArtigoUI(Empresa empresa)
    {
        this.m_empresa = empresa;
        this.m_controllerRA = new ReverArtigoController(empresa);
    }
    
    public void run()
    {
        String strIdRevisor = introduzIdRevisor();
        
        List<Evento> le = m_controllerRA.getEventosSessoesRevisor(strIdRevisor);
        
        apresentarEventosSessoes(strIdRevisor, le);
        
        Evento m_evento = selecionaEvento(le);
        
        List<Revisao> listRevisoes=m_controllerRA.getRevisoesEvento_sessao(m_evento);
        
         apresentaRevisoesEvento(listRevisoes,strIdRevisor);
        
        Revisao rev=selecionaRevisao(listRevisoes);
        if (rev != null)
        {
            m_artigo=m_controllerRA.getArtigoRevisao(rev);
            
            apresentarDadosArtigo(m_artigo);
        }
        else
            System.out.println("Revisão de artigo cancelada.");
        
        Revisao m_revisao = introduzDadosRevisao();
        
        if (m_revisao != null)
        {
            apresentarDadosRevisao(m_revisao);
            
            String m_strConfirma = "Confirma a revisão do artigo com a informação precedente? \n Opção (S/N):";
            boolean bConfirma = confirma(m_strConfirma);
            
            if(bConfirma == true)
            {
                if (m_controllerRA.registarRevisao())
                    System.out.println("Revisao concluida com sucesso.");
                else
                    System.out.println("Revisao cancelada devido a erros.");
            }
        }
        else
            System.out.println("Revisão de artigo cancelada.");
        
    }       
    
    private String introduzIdRevisor()
    {
        return Utils.readLineFromConsole("Introduza Id Revisor: ");
    }
    
    private void apresentarEventosSessoes(String strIdRevisor, List<Evento> le)
    {
        System.out.println("Eventos" + strIdRevisor + ":");
        
        int index = 0;
        for(Evento e : le)
        {
            index++;

            System.out.println(index + ". " + e.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private Evento selecionaEvento(List<Evento> le)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > le.size());

        if( nOpcao == 0 )
            return null;
        else
            return le.get(nOpcao - 1);
    }

    private void apresentarDadosArtigo(Artigo m_artigo) {
        System.out.println(m_artigo.toString());
    }

    private Revisao introduzDadosRevisao()
    {
        String strAvaliacao = Utils.readLineFromConsole("Introduza a Avaliacao: ");
        String strJustificacao = Utils.readLineFromConsole("Introduza a Justificação: ");
        
        return m_controllerRA.setDados(strAvaliacao, strJustificacao);
    }

    private void apresentarDadosRevisao(Revisao m_revisao) {
        System.out.println(m_revisao.toString());
    }
    
    private boolean confirma(String questao) 
    {
        String strConfirma;
        do
        {
            strConfirma = Utils.readLineFromConsole(questao);
        }
        while ( !strConfirma.equalsIgnoreCase("s") && !strConfirma.equalsIgnoreCase("n") );
        
        return strConfirma.equalsIgnoreCase("s");
    }
    public void apresentaRevisoesEvento(List<Revisao> lr,String strIdRevisor){
     System.out.println("Revisões" + strIdRevisor + ":");
        
        int index = 0;
        for(Revisao r : lr)
        {
            index++;

            System.out.println(index + ". " + r.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }
        private Revisao selecionaRevisao(List<Revisao> lr)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > lr.size());

        if( nOpcao == 0 )
            return null;
        else
            return lr.get(nOpcao - 1);
    }

}
