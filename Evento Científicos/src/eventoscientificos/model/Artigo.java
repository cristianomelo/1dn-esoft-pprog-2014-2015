/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;
import eventoscientificos.regista.registoAutor;
import eventoscientificos.regista.registaLicitacao;
/**
 *
 * @author Marco/Cristiano
 */
public class Artigo 
{
    private String m_strTitulo;
    private String m_strResumo;
    private registoAutor r_autor;
    private registaLicitacao r_licitacaolista;
    private Autor m_autorCorrespondente;
    private Licitacao r_licitacao;
    private String m_strFicheiro;
    public Artigo()
    {
    r_autor=new registoAutor();
    r_licitacaolista=new registaLicitacao();
    }
    
    public void setTitulo(String strTitulo)
    {
        this.m_strTitulo = strTitulo;
    }
    
    public void setResumo(String strResumo)
    {
        this.m_strResumo = strResumo;
    }
    
    public registoAutor getRegistoAutor(){
        return r_autor;
    }
   
    public registaLicitacao getRegistoLicitacao(){
        return r_licitacaolista;
    }
    

    public void setAutorCorrespondente(Autor autor)
    {
        this.m_autorCorrespondente=autor;
    }
    
    public void setFicheiro(String strFicheiro)
    {
        this.m_strFicheiro = strFicheiro;
    }
    
    public String getInfo()
    {
        return this.toString();
    }
    
    public boolean valida()
    {
        return true;
    }

    @Override
    public String toString()
    {
        return this.m_strTitulo + "+ ...";
    }

    public registaLicitacao getRegistaLicitacao() {
       return this.r_licitacaolista;
    }

    public Licitacao getLicitacaoRevisor(String email) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
