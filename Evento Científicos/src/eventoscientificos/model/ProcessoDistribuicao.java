/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

/**
 *
 
/**
 *
 * @author Marco/Cristiano
 */
public interface ProcessoDistribuicao {
    public abstract void setMecanismoDistribuicao(Mecanismo m);
    public abstract String distribui(Evento e);
}