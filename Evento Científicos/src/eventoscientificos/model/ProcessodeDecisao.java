/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.util.List;

/**
 *
 * @author Utilizador
 */
public interface ProcessodeDecisao {
    public void setFormula(String formula);
    public List<Submissao> avalia();
    public void notifica(List<Autor> listaAutores);
}
