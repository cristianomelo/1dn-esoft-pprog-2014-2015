package eventoscientificos.model;

import eventoscientificos.regista.RegistaTipoConflitos;
import eventoscientificos.regista.registaUtilizador;
import java.util.*;
import eventoscientificos.regista.registaUtilizador;
import eventoscientificos.regista.registaEvento;
import eventoscientificos.regista.registaMecanismo;
/**
 *
 * @author Marco/Cristiano
 */

public class Empresa
{
    private registaMecanismo r_mecanismo;
    private registaUtilizador r_utilizador;
    private registaEvento r_evento;
    private RegistaTipoConflitos r_ttipoconflitos;
    public Empresa()
    {
        r_mecanismo=new registaMecanismo();
        r_utilizador=new registaUtilizador();
        r_evento=new registaEvento();
        r_ttipoconflitos=new RegistaTipoConflitos();
        //fillInData();
    }

   public registaUtilizador registaUtilizador(){
       return r_utilizador;
   }
   public registaEvento getRegistoEvento(){
       return r_evento;
   }

    public Utilizador getUtilizador(String strId)
    {
        for(Utilizador u:this.r_utilizador)
        {
            String s1 = u.getUsername();
            if(s1.equalsIgnoreCase(strId))
                return u;
        }
        
        /*
        for( Iterator<Utilizador> it = m_listaUtilizadores.listIterator(); it.hasNext(); )
        {
            Utilizador u = it.next();

            if(u.getUsername().equalsIgnoreCase(strId))
                return u;
        }
        */
        
        return null;
    }

    public RegistaTipoConflitos getRegistoConflito() {
    return r_ttipoconflitos;
    }

    public List<String> getFormulas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private TimerTask AlteraEstadoEvento(){
        TimerTask timTask;
        timTask = new TimerTask() {
            
            @Override
            public void run() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
              return timTask;  
    }
    public void schedule(Evento m_evento, String dataFimSub) {
        schedule_1(m_evento, dataFimSub);
        schedule_2(m_evento, dataFimSub);
        schedule_3(m_evento, dataFimSub);
    }

    public void schedule_1(Evento m_evento, String dataFimSub) {
         Timer timerEvento=new Timer();
        timerEvento.schedule(AlteraEstadoEvento(),Date.parse(dataFimSub));
    }
    public void schedule_2(Evento m_evento,String dataFimSub){
         Timer timerEvento2=new Timer();
         timerEvento2.schedule(AlteraEstadoEvento(),Date.parse(dataFimSub));
    }
    public void schedule_3(Evento m_evento,String dataFimSub){
         Timer timerEvento3=new Timer();
         timerEvento3.schedule(AlteraEstadoEvento(),Date.parse(dataFimSub));
    }


}