/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import eventoscientificos.regista.registoAutor;

/**
 *
 * @author Marco/Cristiano
 */
public class Submissao 
{
    private Artigo m_artigo;
    private registoAutor r_autor;
    private EstadosSubmissao m_state;
    public Submissao()
    {
    
    }
    
    public Artigo novoArtigo()
    {
        return new Artigo();
    }
    
    public String getInfo()
    {
        return this.toString();
    }
    public void setEstado(EstadosSubmissao state){
        this.m_state=state;
    }
    public EstadosSubmissao getEstadoSubmissao(){
        return this.m_state;
    }
    public void setArtigo(Artigo artigo)
    {
        this.m_artigo = artigo;
    }
    
    public boolean valida()
    {
        return true;
    }

    
    @Override
    public String toString()
    {
        return "Submissão:\n";
    }

    Artigo getArtigo() {
      return this.m_artigo;
    }
}
