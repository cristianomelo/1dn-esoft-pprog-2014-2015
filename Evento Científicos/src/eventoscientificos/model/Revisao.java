/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.regista.registaRevisao;
import java.util.List;

/**
 *
 * @author Marco/Cristiano
 */
public class Revisao {
    private String str_Avaliacao;
    private String str_Justificacao;
    private Artigo m_artigo;
    private Revisor m_revisor;
    private registaRevisao r_revisao;
    public Revisao()
    {
        
    }
    
    public void setAvaliacao(String strAvaliacao) {
        this.str_Avaliacao = strAvaliacao;
    }

    public void setJustificacao(String strJustificacao) {
        this.str_Justificacao = strJustificacao;
    }
    public void setArtigo(Artigo a){
        this.m_artigo=a;
    }
    public void setRevisor(Revisor r){
        this.m_revisor=r;
    }

    public boolean valida()
    {
        return true;
    }
    
    
}
