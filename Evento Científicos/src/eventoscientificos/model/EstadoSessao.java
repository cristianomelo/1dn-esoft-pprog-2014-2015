/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import estados.SessaoCPDefenidaState;
import estados.SessaoCriadaState;
import estados.SessaoDecedidaState;
import estados.SessaoEmLicitacaoState;
import estados.SessaoEmSubmissaoState;
import estados.SessaoRegistadaState;
import estados.SessaoDetetadaState;
import estados.SeessaoEmRevisaoState;
import estados.SessaoEmDistribuicaoState;

/**
 *
 * @author Utilizador
 */
public interface EstadoSessao {
    public boolean valida();
    public SessaoCriadaState setCriadoState();
    public SessaoRegistadaState setRegistadaState();
    public SessaoCPDefenidaState setCPDefenidaState();
    public SessaoEmSubmissaoState setEmSubmissaoState();
    public SessaoEmLicitacaoState setEmLicitacaoState();
    public SeessaoEmRevisaoState setEmRevisao();
    public SessaoEmDistribuicaoState setEmDistribuicaoState();
    public SessaoDecedidaState setDecedidoState();
    public SessaoDetetadaState setDetetadoState();
    
}
