/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import estados.EventoCPDefenidaState;
import estados.EventoCriadoState;
import estados.EventoDecedidoState;
import estados.EventoDistribuidoState;
import estados.EventoEmRevisaoState;
import estados.EventoDetetadoState;
import estados.EventoDistribuidoState;
import estados.EventoEmLicitacaoState;
import estados.EventoEmSubmissao;
import estados.EventoRegistadoState;
import estados.EventoSessoesDefenidasState;

/**
 *
 * @author Utilizador
 */
public interface EstadosEvento {
    public boolean valida();
    public EventoCriadoState setCriadoState();
    public EventoRegistadoState setRegistadoState();
    public EventoSessoesDefenidasState setSessoesDefenidasState();
    public EventoCPDefenidaState setCPDefenidaState();
    public EventoEmSubmissao setEventoEmSubmissaoState();
    public EventoEmLicitacaoState setEventoEmLicitacaoState();
    public EventoEmRevisaoState setEventoEmRevisaoState();
    public EventoDecedidoState setEventoDecedidoState();
    public EventoDistribuidoState setEventoDistribuidoState();
    public EventoDetetadoState setEventoEmDetecao();
}
