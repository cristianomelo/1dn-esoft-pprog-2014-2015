/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.regista.registaOrganizador;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import eventoscientificos.regista.registaSubmissao;
import eventoscientificos.regista.registaRevisao;
import eventoscientificos.regista.registaSessao;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Marco/Cristiano
 */

public class Evento implements Distribuivel,revisavel,Decisivel,Detetavel,Licitavel
{
    private String m_strTitulo;
    private String m_strDescricao;
    private Local m_local;
    private String m_strDataInicio;
    private EstadosEvento m_state;
    private String m_strDataFim;
    private registaOrganizador r_organizador;
    private ProcessoDecisao m_Decisao;
    private registaSubmissao r_submissao;
    private registaSessao r_sessao;
    private registaRevisao r_revisao;
    private CP m_cp;
    private String dataFimSub;
    private String dataInicioSub;

    public Evento()
    {
        m_local = new Local();
        r_organizador = new registaOrganizador();
        r_submissao=new registaSubmissao();
        r_sessao=new registaSessao();
    }
    
    public CP novaCP()
    {
        m_cp = new CP();
        
        return m_cp;
    }
    
    public void setTitulo(String strTitulo)
    {
        this.m_strTitulo = strTitulo;
    }

    public void setDescricao(String strDescricao)
    {
        this.m_strDescricao = strDescricao;
    }

    public void setDataInicio(String strDataInicio)
    {
        this.m_strDataInicio = strDataInicio;
    }

    public void setDataFim(String strDataFim)
    {
        this.m_strDataFim = strDataFim;
    }

    public void setLocal(String strLocal)
    {
        this.m_local.setLocal(strLocal);
    }

    public String getTitulo() {
        return m_strTitulo;
    }
    
    public registaOrganizador getRegistoOrganizador(){
        return this.r_organizador;
    }
    
    public List<Organizador> getListaOrganizadores()
    {
        List<Organizador> lOrg = new ArrayList<Organizador>();
        
        for( ListIterator<Organizador> it = r_organizador.listIterator(); it.hasNext(); )
        {
            lOrg.add( it.next() );
        }
        
        return lOrg;
    }

    
    public registaSessao getRegistaSessao(){
        
        return r_sessao;
    }



    
    public boolean valida()
    {
        return true;
    }

    public void setCP(CP cp)
    {
        m_cp = cp;
    }
    
    @Override
    public String toString()
    {
        return this.m_strTitulo + "+ ...";
    }

    public boolean aceitaSubmissoes() 
    {
        return true;
    }
    
    public registaSubmissao getRegistoSubmissao(){
        return r_submissao;
    }
   
   @Override
    public ProcessoDistribuicao novoProcessoDistribuicao() {
        return null;
    }

    @Override
    public void setProcessoDistribuicao(ProcessoDistribuicao pd) {
    }
       public List<Artigo> getArtigosRevisor(String idRevisor) {
        List<Artigo> m_artigosRevisor = null;
        return m_artigosRevisor;
    }


    @Override
    public List<Revisao> getRevisoesEvento_sessao(revisavel r) {
        return null;
    }

    @Override
    public List<Decisivel> getEventos_SessoesFaseDecisao(String email) {
        return null;
    }

    @Override
    public ProcessoDecisao novaDecisao() {
       return m_Decisao=new ProcessoDecisao();
    }

    @Override
    public List<Submissao> avalia() {
        return null;
    }

    @Override
    public boolean setProcesso(ProcessoDecisao pd) {
       return true;
    }
    
    @Override
    public boolean setPD(ProcessoDecisao pd) {
     return true;
    }
    public String getDataFimSub(){
        return this.dataFimSub;
    }
    @Override
    public boolean setEstado(EstadosEvento state) {
       this.m_state=state;
       return true;
    }

    @Override
    public void detetarConflito() {
       for(Submissao sub:this.getRegistoSubmissao()){
           Artigo m_Artigo=sub.getArtigo();
           for(Revisor rev:this.m_cp.getRegistoRevsores()){
               ProcessoDetecao pdet=new ProcessoDetecao();
               ConflitoIntresse con=pdet.detetarConflito(rev, m_Artigo);
               if(con!=null){
                   Licitacao liRevisor=m_Artigo.getRegistaLicitacao().getLicitacaoRevisor(rev);
                   if(liRevisor!=null){
                       liRevisor.addConflito(con);
                   }
               }
           }
       }
    }

    @Override
    public List<Licitavel> getEventosSessoesPodeLicitarRevisor(String email) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Artigo> getArtigosLicitavel(Licitavel li) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

        private TimerTask AlteraEstadoSessao(){
        TimerTask timTask;
        timTask = new TimerTask() {
            
            @Override
            public void run() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
              return timTask;  
    }

    public void schedule(Sessao m_sessao, String dataFimSub) {
        schedule_1(m_sessao, dataFimSub);
        schedule_2(m_sessao, dataFimSub);
        schedule_3(m_sessao, dataFimSub);
    }

    public EstadosEvento getEstado() {
       return this.m_state;
    }
 public void schedule_1(Sessao m_sessao, String dataFimSub) {
        Timer timerSessao1=new Timer();
        timerSessao1.schedule(AlteraEstadoSessao(),Date.parse(dataFimSub));
        
    }
    public void schedule_2(Sessao m_sessao,String dataFimSub){
         Timer timerSessao2=new Timer();
         timerSessao2.schedule(AlteraEstadoSessao(),Date.parse(dataFimSub));
    }
    public void schedule_3(Sessao m_sessao,String dataFimSub){
         Timer timerSessao3=new Timer();
         timerSessao3.schedule(AlteraEstadoSessao(),Date.parse(dataFimSub));
    }

 
}
