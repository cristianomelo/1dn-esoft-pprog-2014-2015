/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.regista.registaProponente;
import eventoscientificos.regista.registaSubmissao;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import eventoscientificos.regista.registaSubmissao;

/**
 *
 * @author Marco/Cristiano
 */
public class Sessao implements Distribuivel,revisavel,Decisivel,Detetavel,Licitavel{
    private String m_strCodigo;
    private String m_strDescricao;
    private EstadosEvento m_state;
    private EstadoSessao st_state;
    private ProcessoDecisao m_Decisao;
    private String dataFimSub;
    private registaProponente r_proponente;
    private CP m_cp;
    private registaSubmissao r_submissao;

    public Sessao()
    {
       r_proponente= new registaProponente();
    
    }
        public registaSubmissao getRegistoSubmissao(){
        r_submissao=new registaSubmissao();
        return r_submissao;
    }
     public void setCodigo(String strCodigo)
    {
        this.m_strCodigo = strCodigo;
    }
    public String getDataFimSub(){
        return this.dataFimSub;
    }
    public void setDescricao(String strDescricao)
    {
        this.m_strDescricao = strDescricao;
    }
    @Override
    public String toString()
    {
        return this.m_strDescricao + "+ ...";
    }

    public boolean valida()
    {
        return true;
    }
    public boolean validaSessão(){
        return true;
    }
    public registaProponente getRegistoProponente(){
        return r_proponente;
    }
    public void setCP(CP cp)
    {
        m_cp = cp;
    }

    @Override
    public ProcessoDistribuicao novoProcessoDistribuicao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setProcessoDistribuicao(ProcessoDistribuicao pd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Revisao> getRevisoesEvento_sessao(revisavel r) {
       return null;
    }

    @Override
    public List<Decisivel> getEventos_SessoesFaseDecisao(String email) {
        return null;
    }
    public EstadoSessao getEstadoSessao(){
        return this.st_state;
    }
    @Override
    public ProcessoDecisao novaDecisao() {
        return m_Decisao=new ProcessoDecisao();
    }

    @Override
    public List<Submissao> avalia() {
        return null;
    }

    @Override
    public boolean setProcesso(ProcessoDecisao pd) {
      return true;
    }

    @Override
    public boolean setPD(ProcessoDecisao pd) {
        return true;
    }

    @Override
    public boolean setEstado(EstadosEvento state) {
      m_state=state;
      return true;
    }
    public boolean setEstadoSessao(EstadoSessao st_state){
        this.st_state=st_state;
        return true;
    }
    @Override
    public void detetarConflito() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Licitavel> getEventosSessoesPodeLicitarRevisor(String email) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Artigo> getArtigosLicitavel(Licitavel li) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public CP novaCP() {
        return new CP();
    }
    
    
}
