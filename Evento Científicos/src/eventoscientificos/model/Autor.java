/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

/**
 *
 * @author Marco/Cristiano
 */
public class Autor 
{
    private String m_strNome;
    private String m_strAfiliacao;
    public Autor()
    {
    }
    
    public void setNome(String strNome)
    {
        this.m_strNome = strNome;
    }
    
    public void setAfiliacao(String strAfiliacao)
    {
        this.m_strAfiliacao = strAfiliacao;
    }
    
    public boolean valida()
    {
        return true;
    }

    public  boolean podeSerCorrespondente() 
    {
        return true;
    }
    
    @Override
    public String toString()
    {
        return this.m_strNome + " - " + this.m_strAfiliacao; 
    }
}
