/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import estados.SubmissaoCriadaState;
import estados.SubmissaoDecedidaState;
import estados.SubmissaoDistribuidaState;
import estados.SubmissaoRegistadaState;
import estados.SubmissaoRevistaState;

/**
 *
 * @author Utilizador
 */
public interface EstadosSubmissao {
    public boolean valida();
    public SubmissaoCriadaState setSubmissaoCriadaState();
    public SubmissaoRegistadaState setRegistadaState();
    public SubmissaoRevistaState setSubmissaoRevistaState();
    public SubmissaoDistribuidaState setSubmissaoDistribuidaState();
    public SubmissaoDecedidaState setSubmissaoDecedidaState();
}
