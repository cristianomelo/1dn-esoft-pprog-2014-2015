package eventoscientificos.model;

/**
 *
 * @author Marco/Cristiano
 */

public class Utilizador
{
    private String m_strNome;
    private String m_strUsername;
    private String m_strPassword;
    private String m_strEmail;

    public Utilizador()
    {
    }
    
    public void setNome(String strNome)
    {
        this.m_strNome = strNome;
    }

    public void setUsername(String strUsername)
    {
        m_strUsername = strUsername;
    }
    
    public void setEmail(String strEmail)
    {
        this.m_strEmail = strEmail;
    }
    
    public void setPassword(String strPassword)
    {
        m_strPassword = strPassword;
    }

    public String getNome()
    {
        return m_strNome;
    }
    
    public String getUsername()
    {
        return m_strUsername;
    }
    
    public String getEmail()
    {
        return m_strEmail;
    }
    
    public String getPassword()
    {
        return m_strPassword;
    }
    
    public boolean valida()
    {
        return true;
    }
    
    @Override
    public String toString()
    {
        return "Utilizador:\n" + 
                "\tNome: " + this.m_strNome + "\n" +
                "\tUsername: " + this.m_strUsername + "\n" +
                "\tPassword: " + this.m_strPassword + "\n" +
                "\tEmail: " + this.m_strEmail + "\n";
    }
    
            
}

