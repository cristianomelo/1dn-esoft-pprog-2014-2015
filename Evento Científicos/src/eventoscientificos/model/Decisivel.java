/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.util.List;

/**
 *
 * @author Utilizador
 */
public interface Decisivel {
    public List<Decisivel> getEventos_SessoesFaseDecisao(String email);
    public ProcessoDecisao novaDecisao();
    public List<Submissao> avalia();
    public boolean setProcesso(ProcessoDecisao pd);
    public boolean setPD(ProcessoDecisao pd);
    public boolean setEstado(EstadosEvento state);
}
