package eventoscientificos.model;

import eventoscientificos.regista.registaRevisor;
import java.util.*;

/**
 *
 * @author Marco/Cristiano
 */

public class CP
{
    private registaRevisor r_revisor;

    public CP()
    {
        r_revisor = new registaRevisor();
    }
    public registaRevisor getRegistoRevisor(){
        return this.r_revisor;
    }
    @Override
    public String toString()
    {
        String strCP = "Membros de CP: ";
        for( ListIterator<Revisor> it = r_revisor.listIterator(); it.hasNext(); )
        {
            strCP += it.next().toString();
            if( it.hasNext() )
                strCP += ", ";
        }
        return strCP;
    }

    public Iterable<Revisor> getRegistoRevsores() {
        return this.r_revisor;
    }
}